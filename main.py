#!/usr/bin/env python3
# Soubor:  main.py
# Datum:   01.11.2023 13:30
# Autor:   Marek Nožka, nozka <@t> spseol <d.t> cz
# Autor:   Marek Nožka, marek <@t> tlapicka <d.t> net
# Licence: GNU/GPL
# Úloha:   Pogram pro šifrování a dešifrování Caesarovy šifry
# Popis:
############################################################################
import unicodedata

alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"


def string_shift(text: str, key: int):
    result = ""
    text = text.upper()
    text = unicodedata.normalize("NFKD", text).encode("ascii", "ignore").decode("ascii")
    for char in text:
        if char >= "A" and char <= "Z":
            position = ord(char) - 65
            new_position = (position + key) % 26
            result += chr(new_position+65)
        else:
            continue
        pass
        pass
    return result


print(string_shift("Ahoj    987656 ., ; ABC ; ; ahoj", 3))
print(string_shift("DKRM", -3))
